INSERT INTO Category VALUES (1, 'Suits'), (2, 'Blazers'), (3, 'Shirts'), (4, 'Trousers');

INSERT INTO Image VALUES
(1, 'https://artofstyle.club/wp-content/uploads/2014/03/barney-stinson-suit-up-tips.jpg'),
(2, 'https://www.alux.com/wp-content/uploads/2014/07/10-Best-Dressed-TV-Stars-And-Shows-Barney-Stinson-Neil-Patrick-Harris-How-I-Met-Your-Mother.jpg'),
(3, 'https://pbs.twimg.com/profile_images/1324444827/barney-suit-up_400x400.jpg'),
(4, 'https://i.pinimg.com/originals/d5/c4/0c/d5c40ccf98064036186c76e5ff005fc9.jpg');

INSERT INTO Product(product_id, product_name, category_id, current_price) VALUES
(1, 'Product 1', 1, 500),
(2, 'Product 2', 1, 450),
(3, 'Product 3', 1, 520),
(4, 'Product 4', 1, 530),
(5, 'Product 5', 1, 510),
(21, 'Product 5', 1, 710),
(6, 'Product 1', 2, 500),
(7, 'Product 2', 2, 450),
(8, 'Product 3', 2, 520),
(9, 'Product 4', 2, 530),
(10, 'Product 5', 2, 510),
(11, 'Product 1', 3, 500),
(12, 'Product 2', 3, 450),
(13, 'Product 3', 3, 520),
(14, 'Product 4', 3, 530),
(15, 'Product 5', 3, 510),
(16, 'Product 1', 4, 500),
(17, 'Product 2', 4, 450),
(18, 'Product 3', 4, 520),
(19, 'Product 4', 4, 530),
(20, 'Product 5', 4, 510);