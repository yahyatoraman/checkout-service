package com.ecommerce.resources;

import com.ecommerce.model.KafkaRowDto;
import com.ecommerce.service.Producer;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class TestController {

    private final Producer producer;

    @GetMapping("/publish-dto")
    public void dto() {
        KafkaRowDto dto = new KafkaRowDto();
        dto.setProductId(3L);
        dto.setQuantity(12);
        this.producer.sendDto(dto);
    }

    @GetMapping("/test-test")
    public String test_test() {
        return "myTest";
    }

}
