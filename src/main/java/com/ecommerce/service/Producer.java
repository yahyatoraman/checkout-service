package com.ecommerce.service;

import com.ecommerce.model.KafkaRowDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Producer {

    @Value("${kafka.checkout.topic}")
    private String CHECKOUT_TOPIC;

    private final KafkaTemplate<Object, KafkaRowDto> kafkaTemplate;

    public void sendDto(KafkaRowDto kafkaRowDto){
        System.out.println("earlier2");
        this.kafkaTemplate.send(CHECKOUT_TOPIC, kafkaRowDto);
        System.out.println("later2");
    }

}
